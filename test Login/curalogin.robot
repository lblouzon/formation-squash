*** Settings ***
Documentation    Template robot main suite.
Library    SeleniumLibrary
*** Variables ***
${url}    https://www.godaddy.com/en-ca
${browser}    firefox
*** Test Cases ***
Welcome Page
    # implicit wait #
    Set Selenium Implicit Wait    2 seconds

    # Open firefox browser at GoDaddy website #  
    Open Browser    ${url}    ${browser}

    # Maximize active window #
    Maximize Browser Window

    # Wait for the domain input box to appear on page #
    Wait Until Element Is Visible    name:domainToCheck

    # Test the page title #
    Title Should Be    Domain Names, Websites, Hosting & Online Marketing Tools - GoDaddy CA

    # Write some wild domain name in input box #
    Input Text    name:domainToCheck    eazypeazy

    # Click search button #
    Click Button    xpath=//button[@aria-label='Search Domain']

    # Check exact match found #
    Wait Until Page Contains Element    xpath=//span[@id='eazypeazy.info']
    ${result}    Get WebElement    xpath=//span[@id='eazypeazy.info']
    Element Should be visible ${result} message="Le résultat de la recherche est affiché"

    Close Browser