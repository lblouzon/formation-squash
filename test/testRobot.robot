
*** Settings ***
Resource	squash_resources.resource
Library    SeleniumLibrary

*** Keywords ***
# Test Setup
#    Open Browser ${URL}    ${Browser}
#  Maximize Browser Window 

#Test Teardown
#    Close Browser

*** Test Cases ***
Connection a CURA Health Care
	[Setup]	Test Setup

    Given L'utilisateur est sur la page d'accueil
    When L'utilisateur souhaite prendre un rendez-vous
    Then La page de connexion s'affiche
    When L'utilisateur se connecte
    Then L'utilisateur est connecté sur la page de rendez-vous

	[Teardown]	Test Teardown