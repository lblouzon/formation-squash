# Automation priority: 100
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_4_SETUP}	Get Variable Value	${TEST 4 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_4_SETUP is not None	${__TEST_4_SETUP}

Test Teardown
	${__TEST_4_TEARDOWN}	Get Variable Value	${TEST 4 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_4_TEARDOWN is not None	${__TEST_4_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Standard account creation
	${gender} =	Get Test Param	DS_gender
	${first} =	Get Test Param	DS_first
	${last} =	Get Test Param	DS_last
	${password} =	Get Test Param	DS_password
	${mail} =	Get Test Param	DS_mail
	${birth} =	Get Test Param	DS_birth
	${offers} =	Get Test Param	DS_offers
	${privacy} =	Get Test Param	DS_privacy
	${news} =	Get Test Param	DS_news
	${gpdr} =	Get Test Param	DS_gpdr
	${display} =	Get Test Param	DS_display

	[Setup]	Test Setup

	Given I am on the AccountCreation page
	When I fill AccountCreation fields with gender ${gender} firstName ${first} lastName ${last} password ${password} email ${mail} birthDate ${birth} acceptPartnerOffers ${offers} acceptPrivacyPolicy ${privacy} acceptNewsletter ${news} acceptGdpr ${gpdr} and submit
	And I go to the Home page
	And I sign out
	Then I can successfully sign in with email ${mail} password ${password}
	And The displayed name is ${display}
	And My personal information is gender ${gender} firstName ${first} lastName ${last} email ${mail} birthDate ${birth} acceptPartnerOffers ${offers} acceptPrivacyPolicy "no" acceptNewsletter ${news} acceptGdpr "no"

	[Teardown]	Test Teardown