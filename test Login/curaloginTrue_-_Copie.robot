*** Settings ***
Documentation    Template robot main suite.
Library    SeleniumLibrary
*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com/
${browser}    firefox

*** Keywords ***

Navigate to Website
     # implicit wait #
    Set Selenium Implicit Wait    2 seconds

    # Open firefox browser at GoDaddy website #  
    Open Browser    ${url}    ${browser}

    # Maximize active window #
    Maximize Browser Window

Access Appointment page

    # Click on the "Make appointment button" #

    Click Element    id:btn-make-appointment 

    # Verify the page #

    Title Should Be    CURA Healthcare Service

    Element Should Contain    //h2[contains(text(),'Login')]    Login

Authentication 
# Connection #

    Click Element    id:txt-username
    Input Text    id:txt-username    John Doe
    Input Text    id:txt-password    ThisIsNotAPassword
    Click Element    id:btn-login

    # Verify connection # 

    Element Should Contain    //h2[contains(text(),'Make Appointment')]    Make Appointment

 Make appointment
  

    # Make an appointment # 
    
    Select From List By Value    id=combo_facility    Hongkong CURA Healthcare Center

    Element Should Contain    id=combo_facility    Hongkong CURA Healthcare Center
    
    Select Radio Button    programs    value=Medicaid

    Input Text    id=txt_visit_date    02/07/2022

    Input Text    id=txt_comment    Coucou, ça va ?

    Click Element    id=btn-book-appointment

Teardown 
    Close Browser


*** Test Cases ***

Navigate and book an appointment

    Navigate to Website

    Access Appointment page

    Authentication

    Make appointment

    Teardown
